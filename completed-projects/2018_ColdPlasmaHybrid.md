<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Investigation of Finite Element Methods for a 4D Hybrid Plasma Model

Status: completed 01/2019

## Topic

In this master thesis two different finite element methods have been applied on a four-dimensional hybrid plasma model and implemented in Python in order to compare the
performances of standard finite elements compared to the finite element exterior calculus. The considered hybrid plasma model is a combined kinetic/fluid description for a
magnetized plasma which consists of cold (fluid) and energetic (kinetic) electrons which
move in a stationary, neutralizing background of ions. The model’s key physics content
for wave propagation parallel to a uniform magnetic field is that it predicts the existence
of growing/damped modes due to energy exchange between the energetic electrons and
electromagnetic waves which propagate in the cold background plasma. The major parameter that determines the growth/damping is the anisotropy of the velocity distribution
of the energetic electrons with respect to the direction of the background magnetic field.

## Info

* Project executive(s): Florian Holderied
* Duration: 12 months
* Context: Master thesis
* Funding: none 
* Start date: 02/2018
* End date: 01/2019
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/), Xin Wang

## Links

* [Master thesis](reports/2018_ColdPlasmaHybrid_master_thesis.pdf)
* [Publication](https://www.sciencedirect.com/science/article/pii/S0021999119308137)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)