<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Geometric discretization of the field formulation of drift-kinetic Vlasov-Maxwell equations in the quasi-neutral regime

Status: completed 11/2020

## Topic

On this master’s thesis we have treated the problem of numerically solving a very non-
trivial system of differential equations coming from the theory of gyrokinetics. The
statistics of Vlasov’s equation has been approached with the Particle-In-Cell method, the
Maxwell electromagnetic equations have been solved in the framework of Finite Element
Exterior Calculus, and codes have been written in Python and the library STRUPHY
implementing the discrete systems on a computer.

## Info

* Project executive(s): Mario Ponce Martinez
* Duration: 7 months
* Context: Master thesis
* Funding: none 
* Start date: 05/2020
* End date: 11/2020
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2020_DriftKineticField_master_thesis.pdf)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)