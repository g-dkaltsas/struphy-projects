<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Geometric Methods for a of class Gradient Flows relaxing the Magnetohydrodynamics Equilibria

Status: completed 12/2020

## Topic

This aim of this work is the computation of MHD equilibria using geometric methods,
following an approach presented in the work of Chodura and Schlüter, where the equi-
librium is found by the evolution of an initial magnetic field with a velocity proportional
to the MHD force balance until an equilibrium state is reached. Essentially, the evolution
is described by the transport and the induction equation with a properly chosen velocity.
In the first part of the document, the approach is described and theoretical aspects are
discussed. The remainder of the document is split into the analysis of the transport
equation in 1-D and 3-D and the 3-D induction equation separately. In each section, we
propose a geometric formulation of the equation and then discuss its discretization, where
we apply the Finite Element Exterior Calculus (FEEC) and use Spline Finite Elements
for the discrete spaces. A strong formulation of the transport equation was considered,
whereas for the induction equation, a strong, a weak and a mixed formulation were proposed. In the following, we summarize each part along with the numerical results obtained.

## Info

* Project executive(s): Juan Esteban Suarez
* Duration: 8 months
* Context: Master thesis
* Funding: none 
* Start date: 05/2020
* End date: 12/2020
* Supervisor(s): Florian Hindenlang, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2020_GradientFlowMHD_master_thesis.pdf)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)