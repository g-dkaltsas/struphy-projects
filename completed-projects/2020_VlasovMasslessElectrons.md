<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Geometric Particle-In-Cell discretizations of a plasma hybrid model with kinetic ions and mass-less fluid electrons

Status: completed 04/2023

## Topic

We explore the possibilities of applying structure-preserving numerical methods to a plasma
hybrid model with kinetic ions and mass-less fluid electrons satisfying the quasi-neutrality
relation. The numerical schemes are derived by finite element methods in the framework of
finite element exterior calculus (FEEC) for field variables, Particle-In-Cell (PIC) methods for the
Vlasov equation, and splitting methods in time based on an anti-symmetric bracket proposed.
Conservation properties of energy, quasi-neutrality relation, positivity of density, and divergence-
free property of the magnetic field are given irrespective of the used resolution and metric. Local
quasi-interpolation is used for dealing with the current terms in order to make the proposed
methods more efficient. The implementation has been done in the framework of the Python
package STRUPHY, and has been verified by extensive numerical experiments.

In a second part, we study the canonical momentum based discretizations of a hybrid model with kinetic ions and mass-less electrons. Two equivalent formulations of the hybrid model are presented, in which the vector potentials are in different gauges and the distribution functions depend on canonical momentum (not velocity). Particle-in-cell methods are used for the distribution functions, and the vector potentials are discretized by the finite element methods in the framework of finite element exterior calculus. Splitting methods are used for the time discretizations. It is illustrated that the second formulation is numerically superior and the schemes constructed based on the anti-symmetric bracket proposed have better conservation properties, although the filters can be used to improve the schemes of the first formulation. 

## Info

* Project executive(s): Yingzhe Li
* Duration: 2.5 years
* Context: Post-doc
* Funding: Max Planck Post-doc 
* Start date: 10/2020
* End date: 04/2023
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/), Eric Sonnendrücker

## Links

* [Publication 1](https://www.sciencedirect.com/science/article/pii/S0021999123007660)
* [Publication 2](https://arxiv.org/abs/2301.10097)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)