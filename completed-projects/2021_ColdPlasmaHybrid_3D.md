<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Implementation of a 3D kinetic-fluid hybrid electron plasma model in STRUPHY

Status: completed 03/2022

## Topic

In this thesis, the electron hybrid model for an arbitrary angle between the propagation
direction of the electromagnetic waves and the background magnetic field B0 was investigated at a greater length. The model consists of cold electrons which are described by the
fluid equations for a thermalized plasma and of energetic electrons which are described by
the Vlasov equation ([see equations here](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.ColdPlasmaVlasov)). The two electron species are coupled via the current in Ampere’s law
in the Maxwell equations and assumed to move in a stationary, neutralizing background
of ions. One important property of this system is the conservation of the energy. The
physical aspect was covered by focusing on the investigation of the growing and damping
modes, which result from the wave particle interactions. It became apparent that beside
the anisotropy in the velocity distribution of the energetic electrons also the angle between
the wave vector and the background magnetic field is another important parameter which
determines the growth/damping of the electromagnetic waves. So, beside the Whistler
mode also the other branches of the dispersion relation show a growth/damping effect.

## Info

* Project executive(s): Benedikt Aigner
* Duration: 15 months
* Context: Master thesis
* Funding: none 
* Start date: 01/2021
* End date: 03/2022
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Master thesis](reports/2021_ColdPlasmaHybrid_3D_master_thesis.pdf)


## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)