<img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
# Cold-plasma model in new parallel framework

Status: completed 07/2023

## Topic

We derived a discretization of the hybrid cold plasma model in the GEMPIC framework.
The model consists of Maxwell equations coupled with a fluid equation for cold electrons
and a kinetic description of hot electrons by the Vlasov equation. Field discretization is
done by FEEC on a general curvilinear mapped domain and PIC is used for the kinetic
equation. The resulting semi-discrete system is a non-canonical Hamiltonian system and
we apply a Poisson splitting to conserve the Hamiltonian. Crank-Nicolson steps and solution schemes are outlined for all resulting subsystems. The entire model together with the
two building blocks (pure cold plasma and Vlasov-Maxwell) is implemented in the Struphy (STRUcture-Preserving HYbrid codes) python package. Simple test cases for all three
models are investigated and the results agree well with theoretical expectations.

## Info

* Project executive(s): Joris Thiel
* Duration: 4 months
* Context: Internship
* Funding: none 
* Start date: 04/2023
* End date: 07/2023
* Supervisor(s): [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Report](reports/2023_ColdPlasmaHybrid_3D_report.pdf)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)