 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Vlasov equation with nonlinear adiabatic electrons

Status: open

## Topic

The aim is to implement the Vlasov-Poisson equation (1D-2V formulation) for ions with an adiabatic electron background. 

When invetigating processes with time scale much larger than the time scale of the electron motion (e.g. ion acoustic waves) we can describe the electrons collectively by an appropriate spatial destribution rather than solving the electron Vlasov equation.
The spatial electron distribution can be constructed by assuming that the electron phase-space distribution $f_e$ evolves in a quasi-static way i.e. it satisfies 
the sequilibrium electron Vlasov equation at each time $t. By Jeans theorem, these equilibrium distribution functions should be functions of the electron constants of motion or adiabatic invariants $J$. 

$$
\begin{align}
&\partial_t f + \mathbf v \cdot \nabla f + \frac q m \left(-\nabla \phi + \mathbf v \times \mathbf B_0 \right) \cdot \nabla_{\mathbf v} f = 0\,,
\\[3mm]
&-\epsilon_0 \nabla \cdot \nabla \phi + e n_e(\phi) = q \int f \,\textnormal d \mathbf v\,,
\end{align}
$$
where $n_e$ is the electron spatial density. In non-dimensional form

$$
\begin{align}
&\partial_t f + \mathbf v \cdot \nabla f +  \left(-\nabla \phi + \varepsilon \mathbf v \times \mathbf  b \right) \cdot \nabla_{\mathbf v} f = 0\,,
\\[3mm]
& -\nabla \cdot \nabla \phi + n_e(\phi) =  \int f \,\textnormal d \mathbf v\,.
\end{align}
$$

Here $n_e$ is the normalized electron density (to be determined for various different cases below) and $\varepsilon = \omega_{ci}/\omega_{pi}$ is the ratio of ion cyclotron frequency over the ion plasma frequency. 


In a first phase could maybe linearize the adiabtic relation $n_e=n_e(\phi)$ and use a control variate method:

$$
 - \nabla \cdot \nabla \phi +  \frac{\partial n_e}{\partial \phi}(\phi_0)\phi =  \int f \,\textnormal d \mathbf v\,.
$$



The following cases will be examined:




* $\varepsilon=0$.

 In the absence of a magnetic field the only electron particle constant of motion is the particle energy $$E_e = \frac 1 2 m_e v^2 - e\phi$$. 
 
 One can consider a Maxwellian distribution that describes thermal electrons or a kappa (Vasilyunas) distribution which for suprathermal electrons which is more relevant for space plasmas.



* Electrons in thermal equilibrium.

In thermal equilibrium the electrons have a Maxwellian distribution in velocity space. Thus an appropriate equilibrium solution is 

$$f_e = f_{e0} e^{(m_e v^2/2-e\phi)/(k_B T_e)}.$$ 

Integrating over the entire velocity space we obtain the Boltzmann distribution

$$ n_e(x,t) = n_{0}e^{e\phi/(k_B T_e)},$$

or 

$$n_e=e^{\beta \phi},$$

in non-dimensional form where $\beta = T_i/T_e$ the ratio of the ion to electron temperature.


* Suprathermal electrons




* $\varepsilon\neq 0$ 





## Info

* Project executive(s): open
* Duration: 3-6 months
* Context: Master's thesis / PhD thesis 
* Funding: none
* Start date: open
* End date: open
* Supervisors: [Dimitris Kaltsas](mailto:kaltsas.d.a@gmail.com), [Stefan Possanner](mailto:spossann@ipp.mpg.de)
* Additional information:

## Links

* [Struphy doc](https://struphy.pages.mpcdf.de/struphy/index.html)
* [1] D. A. Kaltsas, P. J. Morrison, and G. N. Throumoulopoulos,  J. Plasma Phys.  89 (4), 905890403 (2023) https://doi.org/10.1017/S0022377823000557 
* [2] G. Knorr and J. Nuehrenberg, Plasma Physics 12, 927 (1970) https://iopscience.iop.org/article/10.1088/0032-1028/12/12/003 

## Contact

* Dimitris Kaltsas [kaltsas.d.a@gmail.com](mailto:kaltsas.d.a@gmail.com)
* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)


 
