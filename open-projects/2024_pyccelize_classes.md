 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Class-based acceleration of Struphy particle kernels

Status: open

## Topic

Since the release of [pyccel 1.11.0](https://github.com/pyccel/pyccel) there is the capability of pyccelizing `objects` and passing them as arguments to other pyccelized functions. This could enable the refactoring of Struphy's particle kernels that are used in PIC simulations for charge deposition and marker pushing.

A review of the current state-of-the-art of marker (or particle) kernels in Struphy is given in [subsection 4](https://struphy.pages.mpcdf.de/struphy/markdown/vlasov-maxwell.html#semi-discretization-in-space) of the Struphy example for [Vlasov-Maxwell-Poisson discretization](https://struphy.pages.mpcdf.de/struphy/markdown/vlasov-maxwell.html#example-vlasov-maxwell-poisson-discretization). Briefly, in such kernels one needs to be able to do two things efficiently:

1. evaluate metric coeffcients at a marker position,
2. evaluate spline fields a a marker positions.

Then there is always a `huge loop over all markers`, in which we can `call pyccelized functions only`. The first point, evaluation of metric coeffcients, is at the moment done via [evaluation_kernels](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/src/struphy/geometry/evaluation_kernels.py?ref_type=heads). In these kernels one needs to hard-code the mapping `f` and its Jacobian determinant `df`, for each of the [available mappings in Struphy](https://struphy.pages.mpcdf.de/struphy/sections/subsections/domains.html). This can be seen by the `if`-statements in the respective functions `f` and `df` of the module, which call `mappings_kernels`, where the hard coding is actually done. Note that the functions `f` and `df` are meant for single-point (or marker) evaluation, hence arrays of position coordinates cannot be passed a this stage.

On the other hand, in Struphy there are the [domain classes](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/src/struphy/geometry/domains.py?ref_type=heads) which comprise all infos for a given Struphy mapping. These are subclasses of the [domain base class](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/src/struphy/geometry/base.py?ref_type=heads), which provides the `__call__` method for the mapping evaluation, and also other methods like `jacobian` etc. for metric coefficients. Indeed, these methods also call the [evaluation_kernels](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/src/struphy/geometry/evaluation_kernels.py?ref_type=heads). However, up to now we are not able to use these `domain objects` in marker kernels, because pyccel did not support classes. However, with the new pyccel capabilites, we envision the use of Struphy's domain classes also in the marker kernels.



## Info

* Project executive(s): open
* Duration: 3 months
* Context: internship
* Funding: none
* Start date: open
* End date: open
* Supervisors: [Yaman Güclü](mailto:Yaman.Guclu@ipp.mpg.de), [Martin Campos-Pinto](mailto:martin.campos-pinto@ipp.mpg.de), [Stefan Possanner](mailto:spossann@ipp.mpg.de)
* Additional information:

## Links

* [Struphy doc](https://struphy.pages.mpcdf.de/struphy/index.html)


 
