 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Electrostatic kinetic electron dynamics in inhomogeneous magnetic and ion backgrounds

Status: open

## Topic

The aim is to implement the Vlasov-Poisson equations (in fixed magnetic field) for electrons in a static ion background:

$$
\begin{align}
&\partial_t f + \mathbf v \cdot \nabla f - \frac e m \left(-\nabla \phi + \mathbf v \times \mathbf B_0 \right) \cdot \nabla_{\mathbf v} f = 0\,,
\\[3mm]
&-\epsilon_0 \nabla \cdot \nabla \phi = \rho_{\textnormal i 0} - e \int f \,\textnormal d \mathbf v\,.
\end{align}
$$


## Info

* Project executive(s): open
* Duration: 3 months
* Context: internship / Master's thesis / PhD thesis / Post-doc
* Funding: none
* Start date: open
* End date: open
* Supervisor: [Stefan Possanner](mailto:spossann@ipp.mpg.de)
* Additional information:

## Links

* [Struphy doc](https://struphy.pages.mpcdf.de/struphy/index.html)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
