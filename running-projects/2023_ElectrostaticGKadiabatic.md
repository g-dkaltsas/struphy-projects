 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Adding a Gyrokinetic Turbulence Model with Adiabatic Electrons in Struphy

Status: running since 12/2023.

## Topic

### Model equations

Implementation of the following gyrokinetic model:

$$
\begin{aligned}
 &\frac{\partial f}{\partial t} + \left[ v_\parallel \frac{\mathbf{B}^*}{B^*_\parallel} + \frac{\mathbf{E}^* \times \mathbf{b}_0}{B^*_\parallel}\right] \cdot \nabla f + \frac{q}{m} \left[ \frac{\mathbf{B}^*}{B^*_\parallel} \cdot \mathbf{E}^*\right] \cdot \frac{\partial f}{\partial v_\parallel} = 0\,,
 \\[5mm]
 &-\nabla_\perp \cdot \left(\frac{m n_0}{|B_0|^2}\right) \nabla_\perp \phi + e n_0 \left(1 + \frac{e}{k_B T_{e0}} \phi \right) = q \int f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu \,,
\end{aligned}
$$

where $f(\mathbf{x}, v_\parallel, \mu, t)$ is the guiding center distribution, $\phi(\mathbf{x})$ denotes the electrostatic potential, $n_0$ and $T_{e0}$ are electron equilibrium density and temperature, respectively, and 

$$
\mathbf{E}^* = -\nabla \phi -\frac \mu q \nabla |B_0| \,,  \qquad \mathbf{B}^* = \mathbf{B}_0 + \frac m q v_\parallel \nabla \times \mathbf{b}_0 \,,\qquad B^*_\parallel = \mathbf B^* \cdot \mathbf b_0  \,.
$$

Here, $\mathbf B_0$ with direction $\mathbf b_0 = \mathbf B_0 / |B_0|$ stands for a static equilibrium magnetic field. Testing will be done with ITG modes in slab and/or Tokamak geometry.

### Lagrangian
The Lagrangian of the above system reads

$$
\begin{equation}
\begin{aligned}
L((\mathbf u_\textrm{gc},a_\parallel), f, \phi) = \int_\Omega \left[ (m v_\parallel\mathbf b_0 + q \mathbf A_0) \cdot \mathbf u_\textrm{gc} - \frac{m v_\parallel^2}{2} - \mu |B_0| - q \phi  \right] f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
\\
+ \int_\Omega \frac{m}{2|B_0|^2} |\nabla_\perp \phi|^2 f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
+ e\int_\Omega \phi n_0 \left(1 + \frac{e}{2k_B T_{e0}} \phi \right)\, \textrm d \mathbf x\,,
\end{aligned}
\end{equation}
$$
where $\nabla \times \mathbf A_0 = \mathbf B_0$ and $(\mathbf u_\textrm{gc}, a_\parallel)$ denote the Eulerian phase space velocity, i.e. the vector field in the ODE

$$
\begin{align}
 \dot{\mathbf x}(t) &= \mathbf u_\textrm{gc}(t, \mathbf x(t), v_\parallel(t))\,,
 \\[2mm]
 \dot{v_\parallel}(t) &= a_\parallel(t, \mathbf x(t), v_\parallel(t))\,.
 \end{align}
$$

The first term in the second line of (1),

$$
 H_2 := - \int_\Omega \frac{m}{2|B_0|^2} |\nabla_\perp \phi|^2 f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x
$$

is a second-order correction coming from the gyrokinetic coordinate transformation.
The function $f^\textrm{vol} := fB^*_\parallel$ is assumed a conserved volume-form transported with the flow of (2)-(3), i.e. 

$$
\begin{equation}
\partial_t f^\textrm{vol} + \nabla \cdot (\mathbf u_\textrm{gc} f^\textrm{vol}) + \frac{\partial}{\partial v_\parallel}(a_\parallel f^\textrm{vol}) = 0\,.
\end{equation}
$$

Moreover, the Jacobian $B^*_\parallel$ of the gyrokinetic coordinate transformation satisfies the Lioville theorem,

$$
\begin{equation}
\partial_t B^*_\parallel + \nabla \cdot (\mathbf u_\textrm{gc} B^*_\parallel) + \frac{\partial}{\partial v_\parallel}(a_\parallel B^*_\parallel) = 0\,,
\end{equation}
$$

which leads to a transport equation for $f$:

$$
\begin{equation}
\partial_t f + \mathbf u_\textrm{gc} \cdot \nabla  f + a_\parallel\frac{\partial f}{\partial v_\parallel} = 0\,.
\end{equation}
$$


Note that the Lagrangian $L$ does not depend on $a_\parallel$.

The action is as usual given by

$$
 I((\mathbf u_\textrm{gc},a_\parallel), f, \phi) = \int_{t_0}^{t_1} L((\mathbf u_\textrm{gc},a_\parallel), f, \phi)\,\mathrm d t
$$

for arbitrary time intervals. The gyrokinetic Poisson equation follows from the Euler-Lagrange equation

$$
\frac{\delta I}{\delta \phi} = 0\,.
$$

The expressions for $(\mathbf u_\textrm{gc}, a_\parallel)$ could be derived from constrained variations as described in eqs. (9)-(10) of the [Vlasov-Poisson project](https://gitlab.mpcdf.mpg.de/struphy/struphy-projects/-/blob/main/running-projects/2024_VlasovPoissonInhomB.md?ref_type=heads). Here, however, we shall take a different approach and derive the equations of motion in Lagrangian coordinates. These are defined as $(\mathbf x_0, v_{\parallel,0})$ in

$$
 (\mathbf x, v_\parallel) = \Phi_t(\mathbf x_0, v_{\parallel,0}) \,,
$$

where $\Phi_t$ is the flow of (2)-(3), i.e. the solution of the ODE system with dependence on the initial condition (which are the Lagrangian coordinates). We can now use that the Lagrange density in (1) is right-invariant (see [Stefan's lecture notes](https://gitlab.mpcdf.mpg.de/spossann/variational-plasma/-/blob/main/lecture_notes.pdf?ref_type=heads), pages 36, Definition 16), and substitute the change of coordinates $(\mathbf x, v_\parallel) =  (\mathcal X_t, \mathcal V_{\parallel, t})$, where $ (\mathcal X_t, \mathcal V_{\parallel, t})$ denotes the space- resp. velocity-components of the flow $\Phi_t$. Noting that the flow satisfies (2)-(3) we have

$$
\begin{align}
 \dot{\mathcal X_t} &= \mathbf u_\textrm{gc}(t, \mathcal X_t, \mathcal V_{\parallel, t})\,,
 \\[2mm]
 \dot{\mathcal V}_{\parallel, t} &= a_\parallel(t, \mathcal X_t, \mathcal V_{\parallel, t})\,,
 \end{align}
$$

such that the Lagrangian becomes

$$
\begin{equation}
\begin{aligned}
L(\Phi_t, \dot \Phi_t, \phi) = \int_\Omega \left[ (m \mathcal V_{\parallel, t}\mathbf b_0(\mathcal X_t) + q \mathbf A_0(\mathcal X_t)) \cdot \dot{\mathcal X}_t - \frac{m \mathcal V_{\parallel, t}^2}{2} - \mu |B_0|(\mathcal X_t) - q \phi(\mathcal X_t)  \right] f_0 B^*_{\parallel,0}\,\textnormal d v_{\parallel,0} \textnormal d \mu_0\, \textrm d \mathbf x_0 
\\
+ \int_\Omega \frac{m}{2|B_0|^2} |\nabla_\perp \phi|^2 f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
+ e\int_\Omega \phi n_0 \left(1 + \frac{e}{2k_B T_{e0}} \phi \right)\, \textrm d \mathbf x\,,
\end{aligned}
\end{equation}
$$

Unconstrained variations with respect to $\Phi_t$ now yield the classical Euler-Lagrange equations for ordinary differential equations,

$$
 \frac{\partial L_\textrm{gc}}{\partial \Phi_t} - \frac{\textrm d}{\textrm d t} \frac{\partial L_\textrm{gc}}{\partial \dot \Phi_t} = 0\,,
$$

where $L_\textrm{gc}$ is the expression in brackets in (9), which is the sole part of $L$ that depends on $\Phi_t$,

$$
 L_\textrm{gc} = (m \mathcal V_{\parallel, t}\mathbf b_0(\mathcal X_t) + q \mathbf A_0(\mathcal X_t)) \cdot \dot{\mathcal X}_t - \frac{m \mathcal V_{\parallel, t}^2}{2} - \mu |B_0|(\mathcal X_t) - q \phi(\mathcal X_t)\,.
$$

We compute the derivatives of the single particle Lagrangian,

$$
\begin{align}
 \frac{\partial L_\textrm{gc}}{\partial \mathcal X_t} &= (m \mathcal V_{\parallel, t} \nabla \mathbf b_0 + q \nabla \mathbf A_0) \cdot \dot{\mathcal X}_t - \mu \nabla |B_0| - q \nabla \phi\,,
 \\[2mm]
 \frac{\partial L_\textrm{gc}}{\partial \dot{\mathcal X}_t} &= m \mathcal V_{\parallel, t}\mathbf b_0(\mathcal X_t) + q \mathbf A_0(\mathcal X_t)\,,
 \\[2mm]
 \frac{\partial L_\textrm{gc}}{\partial \mathcal V_{\parallel,t}} &=  m \mathbf b_0\cdot \dot{\mathcal X}_t - m \mathcal V_{\parallel, t}
 \\[2mm]
 \frac{\partial L_\textrm{gc}}{\partial \dot{\mathcal V}_{\parallel,t}} &= 0\,.
\end{align}
$$

From the last two equations we obtain

$$
\begin{equation}
 \mathbf b_0\cdot \dot{\mathcal X}_t = \mathcal V_{\parallel, t} \,.
 \end{equation}
$$

Moreover, taling the time derivative of (11) yields

$$
 \frac{\textrm d}{\textrm d t}\frac{\partial L_\textrm{gc}}{\partial \dot{\mathcal X}_t} = m \dot{\mathcal V}_{\parallel, t}\mathbf b_0 + m \mathcal V_{\parallel, t} \dot{\mathcal X}_t \cdot \nabla \mathbf b_0  + q \dot{\mathcal X}_t \cdot \nabla \mathbf A_0\,.
$$

By taking the difference of this expression with (10) and using the formula $\nabla \mathbf A \cdot \mathbf b - \mathbf b \cdot \nabla \mathbf A = \mathbf b \times (\nabla \times \mathbf A)$ we obtain

$$
 \dot{\mathcal X}_t \times (m \mathcal V_{\parallel, t} \nabla \times \mathbf b_0 + q \mathbf B_0) - \mu \nabla |B_0| - q \nabla \phi = m \dot{\mathcal V}_{\parallel, t}\mathbf b_0\,.
$$

Let us hence substitute the definitions of $\mathbf E^*$ and $\mathbf B^*$ form above to write this as

$$
\begin{equation}
 q \dot{\mathcal X}_t \times \mathbf B^* + q\mathbf E^* = m \dot{\mathcal V}_{\parallel, t}\mathbf b_0\,.
\end{equation}
$$

We can take the scalar product of this equation with $\mathbf B^*$ to obtain the first equation of motion,

$$
 \dot{\mathcal V}_{\parallel, t} = \frac q m \frac{\mathbf B^*}{B^*_\parallel} \cdot\mathbf E^*\,.
$$

Finally, in (15) we take the cross product with $\mathbf b_0$ form the right,

$$
 (\dot{\mathcal X}_t \times \mathbf B^*) \times \mathbf b_0 + \mathbf E^* \times \mathbf b_0 =0\,,
$$

and use the formula $(\mathbf a \times \mathbf b) \times \mathbf c = (\mathbf c \cdot \mathbf a) \mathbf b - (\mathbf c \cdot \mathbf b) \mathbf a$ to obtain

$$
 \mathbf (b_0 \cdot \dot{\mathcal X}_t)\mathbf B^* - (\mathbf b_0 \cdot \mathbf B^*) \dot{\mathcal X}_t  + \mathbf E^* \times \mathbf b_0 =0\,.
$$

Using now (14) yields the other equations of motion,

$$
 \dot{\mathcal X}_t = \mathcal V_{\parallel, t} \frac{\mathbf{B}^*}{B^*_\parallel} + \frac{\mathbf{E}^* \times \mathbf{b}_0}{B^*_\parallel}\,.
$$


### Noether's theorem

The symmetry of $L$ with respect to time transformations is due to the fact that $L$ has no explicit time dependence, i.e. time only appears through the time-dependence of the unknowns. Hence, the following energy is conserved (see for instance the appendix of [Sugama 2000](https://pubs.aip.org/aip/pop/article-abstract/7/2/466/457925/Gyrokinetic-field-theory)):

$$
W_{\textrm{tot}} = \int \mathbf u_\textrm{gc} \cdot \frac{\partial \mathcal L}{\partial \mathbf u_\textrm{gc}}\, \textrm d \mathbf v\,\textrm d \mathbf x - L\,,
$$

where $\mathcal L$ is the Lagrange density such that

$$
 L = \int \mathcal L\,\textrm d \mathbf v\,\textrm d \mathbf x\,.
$$

For the Lagrangian (1) this leads to

$$
W_{\textrm{tot}} = \int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0| + q \phi  \right) f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
- \int_\Omega \frac{m}{2|B_0|^2} |\nabla_\perp \phi|^2 f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
- e\int_\Omega \phi n_0 \left(1 + \frac{e}{2k_B T_{e0}} \phi \right)\, \textrm d \mathbf x\,.
$$

We can use

$$
 n_0 = \int_\Omega  f_0 B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\,,
$$

and substitute the gyrokinetic Poisson equation to obtain

$$
W_{\textrm{tot}} = \int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0|  \right) f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
+ \int_\Omega \frac{m n_0}{2|B_0|^2} |\nabla_\perp \phi|^2 \, \textrm d \mathbf x 
+ \int_\Omega  \frac{n_0e^2}{2k_B T_{e0}} \phi^2\, \textrm d \mathbf x\,.
$$

We have two important plasma parameters appearing, namely the **Alfvén velocity** $v_\textrm{A}$ and the **Debye length** $\lambda_\textrm{D}$,

$$
 v_\textrm{A} = \frac{|B_0|}{\sqrt{\mu_0 mn_0}} \,,\qquad \lambda_\textrm{D} = \sqrt{\frac{\epsilon_0 k_B T_{e0}}{n_0 e^2}}\,,
$$

which allow us to write the energy as

$$
\begin{equation}
W_{\textrm{tot}} = \int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0|  \right) f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
+ \frac{1}{2\mu_0} \int_\Omega \frac{1}{v_\textrm{A}^2} |\nabla_\perp \phi|^2 \, \textrm d \mathbf x 
+ \frac{\epsilon_0}{2} \int_\Omega  \frac{1}{\lambda^2_\textrm{D}} \phi^2\, \textrm d \mathbf x\,.
\end{equation}
$$

We remarks that $v_\textrm{A}$ and $\lambda_\textrm{D}$ still depend on $\mathbf x$. Likewise, we write the gyrokinetic Poisson equation as

$$
 -\nabla_\perp \cdot \left(\frac{1}{\mu_0 v_\textrm{A}^2}\right) \nabla_\perp \phi + \frac{\epsilon_0}{\lambda_\textrm{D}^2} \phi = q \int f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu - e n_0
$$

Let us verify that this energy is indeed conserved:

$$
\begin{aligned}
\dot W_{\textrm{tot}} &= \int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0|  \right) (\partial_t f B^*_\parallel)\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x 
- \frac{1}{\mu_0} \int_\Omega \phi \partial_t \,\nabla_\perp \cdot\frac{1}{v_\textrm{A}^2}   \nabla_\perp \phi \, \textrm d \mathbf x 
+ \epsilon_0 \int_\Omega  \frac{1}{\lambda^2_\textrm{D}} \phi\,\partial_t \phi\, \textrm d \mathbf x
\\[2mm]
&= \int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0|  \right) (\partial_t f B^*_\parallel)\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x  +  \int_\Omega \phi \partial_t \left(q \int f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu - e n_0\right) \textrm d \mathbf x
\\[2mm]
&=\int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0| +q\phi \right) (\partial_t f B^*_\parallel)\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x
\,.
\end{aligned}
$$

We can now substitute equation (4) and integrate by parts to obtain

$$
\begin{aligned}
\dot W_{\textrm{tot}} &= -\int_\Omega \left( \frac{m v_\parallel^2}{2} + \mu |B_0| +q\phi \right) \left[\nabla \cdot \left(v_\parallel \mathbf B^*f + \mathbf E^* \times \mathbf b_0 f\right)  + \frac qm \partial_{v_\parallel} (\mathbf E^* \cdot \mathbf B^*  f) \right]\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x
\\[2mm]
&= \int_\Omega \left[-q \mathbf E^* \cdot v_\parallel \mathbf B^*f + q v_\parallel \mathbf E^* \cdot \mathbf B^*  f \right]\,\textnormal d v_\parallel \textnormal d \mu\, \textrm d \mathbf x
\\[4mm]
&= 0 \,.
\end{aligned}
$$



## Energy-conserving semi-discretization of gyrokinetic Vlasov-Poisson 

With the [GEMPIC framework of Struphy](https://struphy.pages.mpcdf.de/struphy/sections/discretization.html) it is straigthforward to obtain an energy-conserving semi-discretization in space of our gyrokinetic Vlasov-Poisson system, here re-written for clarity:

$$
\begin{align}
 &\frac{\partial f}{\partial t} + \left[ v_\parallel \frac{\mathbf{B}^*}{B^*_\parallel} + \frac{\mathbf{E}^* \times \mathbf{b}_0}{B^*_\parallel}\right] \cdot \nabla f + \frac{q}{m} \left[ \frac{\mathbf{B}^*}{B^*_\parallel} \cdot \mathbf{E}^*\right] \cdot \frac{\partial f}{\partial v_\parallel} = 0\,,
 \\[5mm]
 &-\nabla_\perp \cdot \left(\frac{1}{\mu_0 v_\textrm{A}^2}\right) \nabla_\perp \phi + \frac{\epsilon_0}{\lambda_\textrm{D}^2} \phi = q \int f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu - e n_0\,,
\end{align}
$$

First, let us write the weak form of the gyrokinetic Poisson equation ($L^2$-scalar product with test function $\psi \in H^1$ and integration by parts on the left):

$$
 \int \frac{1}{\mu_0 v_\textrm{A}^2(\mathbf x)} \nabla_\perp \phi \cdot \nabla_\perp \psi\,\mathrm d \mathbf x + \int \frac{\epsilon_0}{\lambda_\textrm{D}^2(\mathbf x)} \phi\,\psi \,\mathrm d \mathbf x = q\int \int \psi f B^*_\parallel\,\textnormal d v_\parallel \textnormal d \mu \,\mathrm d \mathbf x - e \int n_0(\mathbf x) \psi \,\mathrm d \mathbf x\,,\qquad \forall \ \psi \in H^1\,,
$$

Next, we have to use the mapping $F: \boldsymbol \eta \mapsto \mathbf x$ and the transformation rule of the gradient,

$$
\nabla_x \phi(\mathbf x) = DF^{-\top} \hat\nabla_\eta \hat \phi(\boldsymbol \eta)\,,
$$

where $DF: \boldsymbol \eta \mapsto \mathbb R^{3\times 3}$ denotes the Jacobian matrix of the mapping and $\hat \phi(\boldsymbol \eta) := \phi(F(\boldsymbol \eta))$ is the transformation of 0-forms. The perpendicular gradient is approximated on the logical unit cube to consist only of the first two components of the full gradient,

$$
 \nabla_\perp \phi \approx DF^{-\top} 
 \begin{pmatrix}
 1 & 0 & 0
 \\
 0 & 1 & 0
 \\
 0 & 0 & 0
 \end{pmatrix}
 \hat\nabla_\eta \hat \phi\,.
$$

This is possible because in Struphy, the poloidal plane is always spanned by the first two coordinates $(\eta_1, \eta_2)$, and to a first approximation the magnetic background field can be assumed perpendicular to the poloidal plane.
We can thus pull back the weak Poisson equation to the logical unit cube:

$$
\begin{equation}
 \int \frac{1}{\mu_0 \hat v_\textrm{A}^2(\boldsymbol \eta)} \hat \nabla \hat\phi^\top G^{-1}_\perp \hat \nabla \hat\psi\,\sqrt g\,\mathrm d \boldsymbol \eta + \int \frac{\epsilon_0}{\hat \lambda_\textrm{D}^2(\boldsymbol \eta)} \hat \phi\hat\psi\,\sqrt g \,\mathrm d \boldsymbol \eta = q\int \int \hat \psi \hat f^\textrm{vol} \,\textnormal d v_\parallel \textnormal d \mu \,\mathrm d \boldsymbol \eta - e \int \hat n_0(\boldsymbol \eta) \hat\psi\,\sqrt g \,\mathrm d \boldsymbol \eta\,,\qquad \forall \ \hat \psi \in H^1\,.
\end{equation}
$$

Here, 

$$
G_\perp^{-1} = \begin{pmatrix}
 1 & 0 & 0
 \\
 0 & 1 & 0
 \\
 0 & 0 & 0
 \end{pmatrix} G^{-1} 
 \begin{pmatrix}
 1 & 0 & 0
 \\
 0 & 1 & 0
 \\
 0 & 0 & 0
 \end{pmatrix}\,, \qquad G = DF^\top DF\,,
$$

denotes the "perpendicular components" of the inverse metric tensor $G^{-1}$, $\sqrt g = |\textrm{det}\, DF|$ stands for the Jacobian determinant and $\hat f^{\textrm{vol}} = \hat f \hat B^*_\parallel\sqrt g$ is the volume form corresponding to $f$. The electrostatic potential is discretized as a 0-form:

$$
\hat \phi \approx \hat\phi_h(t, \boldsymbol \eta) = \sum_{i=0}^{N_0 - 1} \phi_i(t)\,\Lambda^0_i(\boldsymbol \eta) = \boldsymbol \phi^\top \boldsymbol \Lambda^0(\boldsymbol \eta) \quad \in \ V^0_h \subset H^1\,.
$$

Its discrete gradient lives in $V^1_h \subset H(\textrm{curl})$ and can be written as

$$
\begin{equation} 
\hat \nabla \hat\phi_h(t, \boldsymbol \eta) = \sum_{i=0}^{N_1-1} (\mathbb G \boldsymbol \phi(t))_i \,\Lambda^1_i(\boldsymbol \eta)\quad \in \ V^1_h \subset H(\textrm{curl})\,,
\end{equation}
$$

where $\mathbb G \in \mathbb R^{N_1 \times N_0}$ is the gradient matrix.
We can thus already write all but one term in (19) in discrete form:

$$
\begin{equation}
\begin{aligned}
 \boldsymbol \psi^\top \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi + \boldsymbol \psi^\top \mathbb M^0_\textrm{ad}\, \boldsymbol \phi  = q \boldsymbol \psi^\top\int \int \boldsymbol\Lambda^0 \hat f^\textrm{vol} \,\textnormal d v_\parallel \textnormal d \mu \,\mathrm d \boldsymbol \eta - e \boldsymbol \psi^\top\int \hat n_0(\boldsymbol \eta) \boldsymbol \Lambda^0\,\sqrt g \,\mathrm d \boldsymbol \eta\,,\qquad \forall \ \boldsymbol\psi \in \mathbb R^{N_0}\,.
 \end{aligned}
 \end{equation}
$$

Here, bold symbols such as $\boldsymbol \phi = (\phi_i)_{i=0}^{N_0 - 1} \in \mathbb R^{N_0}$ indicate vectors, with the Euclidean scalar product denoted by $(\boldsymbol \psi,  \boldsymbol \phi) = \boldsymbol \psi^\top \boldsymbol \phi$ and we introduced the two weighted mass matrices

$$
\begin{align}
 \mathbb M^1_\textrm{gyro} &= \int \frac{1}{\mu_0 \hat v_\textrm{A}^2(\boldsymbol \eta)} \boldsymbol\Lambda^1 G_\perp^{-1} (\boldsymbol \Lambda^1)^\top\,\sqrt g\,\mathrm d \boldsymbol \eta \qquad \in \mathbb R^{N_1 \times N_1}\,,
 \\[4mm]
 \mathbb M^0_\textrm{ad} &= \int \frac{\epsilon_0}{\hat \lambda_\textrm{D}^2(\boldsymbol \eta)}\boldsymbol\Lambda^0 (\boldsymbol \Lambda^0)^\top\,\sqrt g\,\mathrm d \boldsymbol \eta \qquad \in \mathbb R^{N_0 \times N_0}
 \,.
\end{align}
$$

The guiding centers are discretized with $N$ particles:

$$
\hat f^{\textrm{vol}} \approx \hat f^{\textrm{vol}}_h(t, \boldsymbol \eta, v_\parallel) = \frac 1 N\sum_{k=0}^{N-1} w_k \,\delta(\boldsymbol \eta - \boldsymbol \eta_k(t)) \,\delta(v_\parallel - v_{\parallel,k}(t))\,,
$$

where $(\boldsymbol \eta_k(t), v_{\parallel,k}(t))$ satisfy the ODEs (2)-(3) on the logical domain:

$$
\begin{align}
 \dot{\boldsymbol \eta}_k &= DF^{-1}\left(v_{\parallel,k} \frac{DF \hat{\mathbf B}^{*2}}{\sqrt g \hat B^*_\parallel} + \frac{DF^{-\top}\hat{\mathbf E}^{*1} \times DF^{-\top} \hat{\mathbf b}_0^1}{\hat B^*_\parallel}\right)\,,
 \\[4mm]
 \dot{v}_{\parallel,k} &= \frac q m \frac{DF^{-\top}\hat{\mathbf E}^{*1} \cdot DF\hat{\mathbf B}^{*2}}{\sqrt g \hat B^*_\parallel} \,,
\end{align}
$$

where we used the following pull-back formulas:

$$
 \begin{aligned}
 \mathbf B^* &= \frac{DF}{\sqrt g} \hat{\mathbf B}^{*2} \qquad \textrm{(2-form pullback)}\,,
 \\[2mm]
 \mathbf E^* &= DF^{-\top} \hat{\mathbf E}^{*1} \qquad \textrm{(1-form pullback)}\,,
 \\[2mm]
 \mathbf b_0 &= DF^{-\top} \hat{\mathbf b}_0^{*1} \qquad \textrm{(1-form pullback)}\,,
 \\[2mm]
 B^*_\parallel(F(\boldsymbol \eta)) &= \hat B^*_\parallel(\boldsymbol \eta) \qquad \textrm{(0-form pullback)}\,.
 \end{aligned}
$$

Moreover, by using the formula $M\mathbf a \times M\mathbf b = \textrm{det}(M)M^{-\top}(\mathbf a \times \mathbf b)$ and by substituting the 3-form representation $\hat B^{*3}_\parallel = \sqrt g \hat B^*_\parallel$ in (23)-(24) we obtain the final version

$$
\begin{align}
 \dot{\boldsymbol \eta}_k &=  v_{\parallel,k}\frac{\hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} + \frac{\hat{\mathbf E}^{*1} \times \hat{\mathbf b}_0^1}{\hat B^{*3}_\parallel}\,,
 \\[4mm]
 \dot{v}_{\parallel,k} &= \frac q m \frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} \,,
\end{align}
$$

The coupling term in the discrete Poisson equation (20) becomes

$$
 q \boldsymbol \psi^\top\int \int \boldsymbol\Lambda^0 \hat f^\textrm{vol} \,\textnormal d v_\parallel \textnormal d \mu \,\mathrm d \boldsymbol \eta \approx  q\boldsymbol \psi^\top \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t))\,.
$$

Hence, the discrete Poisson equation reads

$$
\begin{equation}
\begin{aligned}
  \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi + \mathbb M^0_\textrm{ad}\, \boldsymbol \phi  = q \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t)) - e \int \hat n_0(\boldsymbol \eta) \boldsymbol \Lambda^0\,\sqrt g \,\mathrm d \boldsymbol \eta\,.
 \end{aligned}
 \end{equation}
$$

The discrete version of the energy (16) reads 

$$
\begin{equation}
W_{\textrm{tot}}(t) = \frac 1N \sum_{k=1}^{N-1} w_k \left(\frac{m v_{\parallel,k}(t)^2}{2} + \mu_k|\hat B_0|(\boldsymbol \eta_k(t)) \right)
+ \frac{1}{2} \boldsymbol \phi^\top(t) \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi(t) 
+ \frac{1}{2} \boldsymbol \phi^\top(t) \mathbb M^0_\textrm{ad}\, \boldsymbol \phi(t) \,.
\end{equation}
$$

Let us verify that this semi-discrete energy is indeed conserved:

$$
\begin{aligned}
\dot W_{\textrm{tot},h} &= \frac 1 N \sum_{k=1}^{N-1} w_k \left(m\,v_{\parallel,k}  \dot{v}_{\parallel,k} + \mu_k \dot{\boldsymbol \eta}_k \cdot \nabla |\hat B_0| \right) + \boldsymbol \phi^\top \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \dot{\boldsymbol \phi} 
+  \boldsymbol \phi^\top(t) \mathbb M^0_\textrm{ad}\, \dot{\boldsymbol \phi}
\\[2mm]
&=\frac 1 N \sum_{k=1}^{N-1} w_k \left(v_{\parallel,k}  q\frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} + \mu_k \dot{\boldsymbol \eta}_k \cdot \nabla |\hat B_0| \right) + \boldsymbol \phi^\top \frac{\mathrm d}{\mathrm d t} \left( q \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t)) - e \int \hat n_0(\boldsymbol \eta) \boldsymbol \Lambda^0\,\sqrt g \,\mathrm d \boldsymbol \eta \right)
\\[2mm]
&=\frac 1 N \sum_{k=1}^{N-1} w_k \left(v_{\parallel,k}  q\frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} + \mu_k \dot{\boldsymbol \eta}_k \cdot \nabla |\hat B_0| \right) + \boldsymbol \phi^\top  q \frac 1 N \sum_{k=0}^{N-1} w_k \dot{\boldsymbol \eta}_k \cdot \nabla \boldsymbol \Lambda^0  
\\[2mm]
&=\frac 1 N \sum_{k=1}^{N-1} w_k \left( \,v_{\parallel,k} q  \frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} +  \dot{\boldsymbol \eta}_k \cdot \mu_k \nabla |\hat B_0| +  q\dot{\boldsymbol \eta}_k \cdot \nabla \hat \phi  \right)
\\[2mm]
&=q\frac 1 N \sum_{k=1}^{N-1} w_k \left( \,v_{\parallel,k}  \frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} - \left( v_{\parallel,k}\frac{\hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} + \frac{\hat{\mathbf E}^{*1} \times \hat{\mathbf b}_0^1}{\hat B^{*3}_\parallel}\right) \cdot \hat{\mathbf E}^{*1} \right)
\\[4mm]
&= 0\,.
\end{aligned}
$$

## Hamiltonian structure

It is straightforward to write the gyrokinetic Vlasov-Poisson system as a non-canonical Hamiltonian system in the variables $\mathbf X = (\boldsymbol \eta_k)_{k=0}^{N-1} \in \mathbb R^{3N}$ and $\mathbf{V}_\parallel = (v_{\parallel,k})_{k=0}^{N-1} \in \mathbb R^N$, where the electric potantial $\phi(\mathbf X)$ is viewed as a complicated function of $\mathbf X$ related to the inverse gyrokinetic Poisson equation. The discrete energy (29) is the Hamiltonian,  written as

$$
\begin{equation}
H(\mathbf X, \mathbf{V}_\parallel) = \frac m2 \mathbf{V}_\parallel^\top \mathbb W \mathbf{V}_\parallel + \mathbf w^\top \mathbf M(\mathbf X) + \frac{1}{2} \boldsymbol \phi^\top(\mathbf X) \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi(\mathbf X) 
+ \frac{1}{2} \boldsymbol \phi^\top(\mathbf X) \mathbb M^0_\textrm{ad}\, \boldsymbol \phi(\mathbf X) \,,
\end{equation}
$$

where 

$$
\mathbb W = \textrm{diag}(\mathbf w) \in \mathbb R^{N \times N}\,,\qquad \mathbf w = \left( \frac{w_k}{N} \right)_{k=0}^{N-1} \in \mathbb R^N\qquad \mathbf M(\mathbf X) = \left( \mu_k |\hat B_0|(\boldsymbol \eta_k) \right)_{k=0}^{N-1} \in \mathbb R^N\,. 
$$

Moreover, the discrete Poisson equation (28) is written as

$$
\begin{equation}
\begin{aligned}
  \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi + \mathbb M^0_\textrm{ad}\, \boldsymbol \phi  = q \mathbb L^0(\mathbf X) \mathbf w - e \mathbf n_0\,.
 \end{aligned}
 \end{equation}
$$

with 

$$
 \mathbf n_0 = \int \hat n_0(\boldsymbol \eta) \boldsymbol \Lambda^0\,\sqrt g \,\mathrm d \boldsymbol \eta \in \mathbb R^{N_0}\,,\qquad \mathbb L^0(\mathbf X) \in \mathbb R^{N_0 \times N}\,,\qquad \mathbb L^0_{ik}(\mathbf X) = \Lambda^0_i(\boldsymbol \eta_k) \in \mathbb R\,.
$$

The gradient of the Hamiltonian (30) with respect to $\mathbf X$ can be computed with the help of the discrete Poisson equation (31),

$$
\begin{aligned}
 \frac{\partial}{\partial \mathbf X} H(\mathbf X, \mathbf{V}_\parallel) &= \mathbb N \frac{\partial}{\partial \mathbf X} \mathbf M(\mathbf X) + \boldsymbol \phi^\top(\mathbf X)\frac{\partial}{\partial \mathbf X}\left( \mathbb G^\top\mathbb M^1_\textrm{gyro}\mathbb G\, \boldsymbol \phi + \mathbb M^0_\textrm{ad}\, \boldsymbol \phi \right) 
 \\[2mm]
 &=  \mathbb N \frac{\partial}{\partial \mathbf X} \mathbf M(\mathbf X) + q\boldsymbol \phi^\top(\mathbf X)\frac{\partial}{\partial \mathbf X} \mathbb L^0(\mathbf X) \mathbf w \,.
\end{aligned}
$$

where $\mathbb N = \textrm{diag}(\mathbf w) \otimes \mathbb I_{3\times 3} \in \mathbb R^{3N \times 3N}$ is a convenient way of expressing the multiplication with the weigths. In order to compute the last term, let us view $\boldsymbol \phi^\top(\mathbf X) = \boldsymbol \phi^\top$ as independent of $\mathbf X$ to see the index contraction, then

$$
\begin{aligned}
 \frac{\partial}{\partial \boldsymbol \eta_i} (\boldsymbol \phi^\top \mathbb L^0(\mathbf X) \mathbf w) &= \frac{\partial}{\partial \boldsymbol \eta_i} \sum_{j,k} \phi_j \Lambda^0_j(\boldsymbol \eta_k) w_k
 \\[2mm]
 &=  \frac{\partial}{\partial \boldsymbol \eta_i} \sum_{k} \hat \phi(\boldsymbol \eta_k) w_k
 \\[2mm]
 &=  \sum_{k} \delta_{i,k} \nabla \hat \phi(\boldsymbol \eta_k) w_k
 \\[2mm]
 &=  \nabla \hat \phi(\boldsymbol \eta_i) w_i\,.
\end{aligned}
$$

Hence we arrive at

$$
 \frac{\partial}{\partial \mathbf X} H(\mathbf X, \mathbf{V}_\parallel) =  \mathbb N \frac{\partial}{\partial \mathbf X} \mathbf M(\mathbf X) + q(\mathbb G \boldsymbol \phi(\mathbf X))^\top\mathbb L^1(\mathbf X)\, \mathbb N\,,

$$

where

$$
\mathbb L^1(\mathbf X) \in \mathbb R^{N_1 \times N \times 3}\,,\qquad \mathbb L^1_{ik} = \Lambda^1_i(\boldsymbol \eta_k) \in \mathbb R^3 \ (\textnormal{vector-valued basis functions})\,.
$$

With this we can compute the gradient of the Hamiltonian w.r.t $(\mathbf X, \mathbf{V}_\parallel)$,

$$
\nabla H(\mathbf X, \mathbf{V}_\parallel) = 
\begin{pmatrix}
 \mathbb N \frac{\partial}{\partial \mathbf X} \mathbf M(\mathbf X) + q(\mathbb G \boldsymbol \phi(\mathbf X))^\top\mathbb L^1(\mathbf X)\, \mathbb N 
\\[1mm]
m \mathbb W \mathbf{V}_\parallel
\end{pmatrix}\,.
$$

Hence, we are able to write the equations of motion (26)-(27) as a non-canonical Hamiltonian system:

$$
\begin{equation}
 \begin{pmatrix}
 \dot{\mathbf X} \\
 \dot{\mathbf{V}}_\parallel
  \end{pmatrix} = 
  \begin{pmatrix}
   \frac 1q b_{0\times}(\mathbf X, \mathbf{V}_\parallel) \mathbb N^{-1} & \frac 1 m  \frac{\hat{\mathbf B}^{*2}(\mathbf X, \mathbf{V}_\parallel)}{\hat B^{*3}_\parallel(\mathbf X, \mathbf{V}_\parallel)} \,\mathbb W^{-1}
   \\[1mm]
   - \frac 1 m  \frac{\hat{\mathbf B}^{*2\top}(\mathbf X, \mathbf{V}_\parallel)}{\hat B^{*3}_\parallel(\mathbf X, \mathbf{V}_\parallel)} \,\mathbb W^{-1}  & 0
  \end{pmatrix}
  \nabla H(\mathbf X, \mathbf{V}_\parallel)\,,
\end{equation}
$$
where the diagonal block is the skew-symmetric rotation matrix

$$
b_{0\times}(\mathbf X) =  \textrm{diag}\left(  
\frac{1}{\hat B^{*3}_\parallel(\boldsymbol \eta_k, v_{\parallel, k})}\begin{pmatrix}
0 & -b^1_{0,3}(\boldsymbol \eta_k) & b^1_{0,2}(\boldsymbol \eta_k)
\\[1mm]
b^1_{0,3}(\boldsymbol \eta_k) & 0 & -b^1_{0,1}(\boldsymbol \eta_k)
\\[1mm]
-b^1_{0,2}(\boldsymbol \eta_k) & b^1_{0,1}(\boldsymbol \eta_k) & 0 
\end{pmatrix}
\right) \in \mathbb R^{3N \times 3N}\,. 
$$


## Time discretization 

The insight that the gyrokinetic Vlasov-Poisson system has the Hamiltonian structure (32) can help with the time discretization. We could leverage many techniques for discretizing non-canonical Hamiltonian ODEs (also called "Poisson systems") that lead to structure-preserving algorithms. Unfortunately, our system (23) has the most general form of a Poisson system; denoting $\mathbf Z = (\mathbf X, \mathbf{V}_\parallel)$ we have

$$
\dot{\mathbf Z} = \mathbb J(\mathbf Z) \nabla H(\mathbf Z)\,,
$$

where the Poisson matrix $\mathbb J(\mathbf Z)$ depends non-linearly on $\mathbf Z$. A Poisson integrator (i.e. a Poisson map that respects all Casimirs of $\mathbb J(\mathbf Z)$, see for instance [Hairer, Lubich, Wanner](https://link.springer.com/book/10.1007/3-540-30666-8)) will thus be very hard to obtain, and we don't know how exactly to do that (it is not the purpose of this project). However, we can still go for exact energy conservation with the **discrete gradient method** (see [chapter 8.2 of Michael_2019 lecture notes](https://datashare.mpcdf.mpg.de/apps/files/?dir=/struphy/reference_books&fileid=231001917#pdfviewer)). For this, our first strategy will be to split the Poisson matrix as

$$
 \mathbb J(\mathbf Z) = \mathbb J_1(\mathbf Z) + \mathbb J_2(\mathbf Z)
$$

such that

$$
\begin{aligned}
 \mathbb J_1(\mathbf Z) & = \begin{pmatrix}
   \frac 1q b_{0\times}(\mathbf Z) \mathbb N^{-1} & 0
   \\[1mm]
   0  & 0
  \end{pmatrix}
  \\[4mm]
  \mathbb J_2(\mathbf Z) &= \begin{pmatrix}
   0 & \frac 1 m  \frac{\hat{\mathbf B}^{*2}(\mathbf Z)}{\hat B^{*3}_\parallel(\mathbf Z)} \,\mathbb W^{-1}
   \\[1mm]
   - \frac 1 m  \frac{\hat{\mathbf B}^{*2\top}(\mathbf Z)}{\hat B^{*3}_\parallel(\mathbf Z)} \,\mathbb W^{-1}  & 0
  \end{pmatrix}\,.
\end{aligned}
$$

The first splitting step of our scheme is thus a discrete version of

$$
\begin{equation}
 \begin{pmatrix}
 \dot{\mathbf X} \\
 \dot{\mathbf{V}}_\parallel
  \end{pmatrix} =  
  \begin{pmatrix}
   \frac 1q b_{0\times}(\mathbf Z) \mathbb N^{-1} & 0
   \\[1mm]
   0  & 0
  \end{pmatrix}
  \nabla H(\mathbf Z)\,,
\end{equation}
$$

which correspsonds to

$$
 \dot{\boldsymbol \eta}_k = \frac{\hat{\mathbf E}^{*1}(\boldsymbol \eta_k) \times \hat{\mathbf b}_0^1(\boldsymbol \eta_k)}{\hat B^{*3}_\parallel(\boldsymbol \eta_k, v_{\parallel, k})}\,,\qquad\quad  \hat{\mathbf E}^{*1} = -\nabla \hat\phi -\frac{\mu_k}{q} \nabla |\hat B_0|\,.
$$

The second splitting step is a discrete version of 

$$
\begin{equation}
 \begin{pmatrix}
 \dot{\mathbf X} \\
 \dot{\mathbf{V}}_\parallel
  \end{pmatrix} =  
  \begin{pmatrix}
   0 & \frac 1 m  \frac{\hat{\mathbf B}^{*2}(\mathbf Z)}{\hat B^{*3}_\parallel(\mathbf Z)} \,\mathbb W^{-1}
   \\[1mm]
   - \frac 1 m  \frac{\hat{\mathbf B}^{*2\top}(\mathbf Z)}{\hat B^{*3}_\parallel(\mathbf Z)} \,\mathbb W^{-1}  & 0
  \end{pmatrix}
  \nabla H(\mathbf Z)\,,
\end{equation}
$$

which corresponds to

$$
\begin{align}
 \dot{\boldsymbol \eta}_k &=  v_{\parallel,k}\frac{\hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} \,, \qquad\quad  \hat{\mathbf B}^{*2} = \hat{\mathbf{B}}^2_0 + \frac m q v_{\parallel,k} \nabla \times \hat{\mathbf{b}}^1_0 \,,\qquad \hat B^{*3}_\parallel = |\hat B_0^3| + \sqrt g\,\frac m q v_{\parallel,k} \hat{\mathbf b}^1_0 \cdot(\nabla \times \hat{\mathbf{b}}_0)_\textrm{vec}
 \\[4mm]
 \dot{v}_{\parallel,k} &= \frac q m \frac{\hat{\mathbf E}^{*1} \cdot \hat{\mathbf B}^{*2}}{\hat B^{*3}_\parallel} \,, \qquad\quad  \hat{\mathbf E}^{*1} = -\nabla \hat\phi -\frac{\mu_k}{q} \nabla |\hat B_0|
\end{align}
$$


### Discrete gradient method

The discrete gradient method features exact energy conservation. We shall implement the Gonzalez discrete gradient method:

$$
\begin{equation}
 \frac{\mathbf Z^{n+1} - \mathbf Z^n}{\Delta t} = \mathbb J(\mathbf Z^n) \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^n)\,,
\end{equation}
$$

where

$$
\begin{equation}
 \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^n) = \nabla H(\mathbf Z^{n+1/2}) + (\mathbf Z^{n+1} - \mathbf Z^n) \frac{H(\mathbf Z^{n+1}) - H(\mathbf Z^n) - (\mathbf Z^{n+1} - \mathbf Z^{n+1})^\top \nabla H(\mathbf Z^{n+1/2}) }{||\mathbf Z^{n+1} - \mathbf Z^n||}\,,
\end{equation}
$$

and $\mathbf Z^{n+1/2} = (\mathbf Z^{n+1}  +\mathbf Z^{n})/2$.
Like all discrete gradients, but here particularly easy to see, the Gonzalez discrete gradient satisfies

$$
 (\mathbf Z^{n+1} - \mathbf Z^{n+1}) \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^{n}) = H(\mathbf Z^{n+1}) - H(\mathbf Z^n)\,,
$$

which immediatly implies exact conservation of $H$ through the skew symmetry of $\mathbb J$. 

### Existing guiding center propagators

We can build on these two propagators:

https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_markers.PushGuidingCenterBstar

https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_markers.PushGuidingCenterbxEstar


## Info

* Project executive(s): Ipek Akbatur
* Duration: ongoing
* Context: Master thesis
* Funding: none
* Start date: 12/2023
* End date: open
* Supervisor: [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links



## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
