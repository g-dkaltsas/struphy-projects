 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Implementation of various nonlinear fluid models from variational principles, possible coupling to kinetic species

Status: running since 11/2023.

## Topic

The aim is to implement a sequence of nonlinear fluid models, one building upon the other, whose discretizations are obtained from Eulerian variational principles. The final goal is to have a nonlinear version of full, ideal MHD equations in Struphy, satisfying several discrete concervation laws such as energy, momentum, $\nabla \cdot \mathbf B =0$ and magnetic helicity. The sequence of models is as follows:

1. [VariationalBurgers](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.VariationalBurgers)
2. [VariationalPressurelessFluid](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.VariationalPressurelessFluid)
3. [VariationalBarotropicFluid](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.VariationalBarotropicFluid)
4. [VariationalCompressibleFluid](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.VariationalPressurelessFluid)
5. [VariationalMHD](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.fluid.VariationalMHD)


## Info

* Project executive(s): Valentin Carlier
* Duration: ongoing
* Context: part of PhD thesis
* Funding: ?
* Start date: 11/2023
* End date: open
* Supervisor: Martin Campos Pinto, Yaman Güclü, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Variational toy models](https://struphy.pages.mpcdf.de/struphy/sections/STUBDIR/struphy.models.toy.html)
* [Preprint on variational schemes](https://arxiv.org/abs/2402.02905)

## Contact

* Valentin Carlier [valentin.carlier@ipp.mpg.de](mailto:valentin.carlier@ipp.mpg.de)
* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
