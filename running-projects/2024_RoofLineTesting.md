 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Testing the performance and efficiency of various Struphy models on MPCDF computing clusters

Status: running since 01/2024.

## Topic

The aim is to assess the performance and efficiency of Struphy on single computing nodes before scaling to larger simulations.
Testing methods will include the roofline model, among others.


## Info

* Project executive(s): Max Lindquist
* Duration: ongoing
* Context: ACH Post-doc
* Funding: ACH
* Start date: 01/2024
* End date: ongoing
* Supervisor: Roman Hatzky, [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links


## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
