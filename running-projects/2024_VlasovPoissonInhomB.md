 <img src="../dog-cartoon-struphy.jpg" width="150" align="right"> 
 
 # Electrostatic full-orbit electron plasma in inhomogeneous background

Status: running since 01/2024

## Topic

### Model equations
The aim is to implement the Vlasov-Poisson equations (in fixed magnetic field) for electrons in a static ion background:

$$ 
\begin{align}
&\partial_t f + \mathbf v \cdot \nabla f - \frac e m \left(-\nabla \phi + \mathbf v \times \mathbf B_0 \right) \cdot \nabla_v f = 0\,,
\\[3mm]
&-\epsilon_0 \nabla \cdot \nabla \phi = \rho_{\textrm i 0} - e \int f \,\textnormal d \mathbf v\,.
\end{align}
$$

Here, $B_0(\mathbf x)$ and $\rho_{\textrm i 0}(\mathbf x)$ are static, inhomogeneous magnetic field and ion charge density, respectively. An equivalent model in terms of the electric field $\mathbf E$ rather than the potential $\phi$ reads

$$
\begin{align}
&\partial_t f + \mathbf v \cdot \nabla f - \frac e m \left(\mathbf E + \mathbf v \times \mathbf B_0 \right) \cdot \nabla_v f = 0\,,
\\[3mm]
&-\frac{1}{c^2} \partial_t \mathbf E + \nabla \times \mathbf B_0 = -\mu_0 e \int \mathbf v\, f \,\textnormal d \mathbf v\,,
\end{align}
$$
where it is assumed that the equilibrium current is carried solely by electrons ($\mathbf j_{\textnormal i 0} = 0$). In this model the Poisson equation (2) needs to be solved once at the start of the simulation to deterine the initial electric field.

Our goal is to implement both models for arbitrary backgrounds available in Struphy.

### Lagrangian with $\phi$
The Lagrangian of system (1)-(2) reads

$$
\begin{equation}
\begin{aligned}
L((\mathbf u,\mathbf a), f, \phi) = \int_\Omega \left[ (m\mathbf v - e \mathbf A_0) \cdot \mathbf u - \frac{m|\mathbf v|^2}{2} + e \phi \right] f\, \textrm d \mathbf v\, \textrm d \mathbf x 
\\
+ \frac{\epsilon_0}{2}\int_\Omega |\nabla \phi|^2 \textrm d \mathbf x - \int_\Omega \phi\,\rho_{\textrm i 0}\, \textrm d \mathbf x\,,
\end{aligned}
\end{equation}
$$
where $\nabla \times \mathbf A_0 = \mathbf B_0$ and $(\mathbf u, \mathbf a)$ denote the Eulerian phase space velocity, i.e. the vector field in

$$
\begin{align}
 \dot{\mathbf x}(t) &= \mathbf u(t, \mathbf x, \mathbf v)\,,
 \\[2mm]
 \dot{\mathbf v}(t) &= \mathbf a(t, \mathbf x, \mathbf v)\,.
 \end{align}
$$

The function $f$ is assumed a conserved volume-form transported with the flow of (6)-(7), i.e. 

$$
\begin{equation}
\partial_t f + \nabla_x \cdot (\mathbf u f) + \nabla_v \cdot(\mathbf a f) = 0\,.
\end{equation}
$$
Note that the Lagrangian $L$ does not depend on $\mathbf a$.

The action is as usual given by

$$
 I((\mathbf u,\mathbf a), f, \phi) = \int_{t_0}^{t_1} L((\mathbf u,\mathbf a), f, \phi)\,\mathrm d t
$$

for arbitrary time intervals. The Poisson equation follows from the Euler-Lagrange equation

$$
\frac{\delta I}{\delta \phi} = 0\,.
$$

In order to obtain $\mathbf u$ and $\mathbf a$ one has to take constrained variations with respect to $(\mathbf u, \mathbf a)$ and $f$ (see [Stefan's lecture notes](https://gitlab.mpcdf.mpg.de/spossann/variational-plasma/-/blob/main/lecture_notes.pdf?ref_type=heads), pages 36-37, Theorem 15), of the following form:

$$
\begin{align}
\delta \mathbf u &= \dot{\boldsymbol \eta}_x + \mathbf u \cdot \nabla_x \boldsymbol \eta_x + \mathbf a \cdot \nabla_v \boldsymbol \eta_x - \boldsymbol \eta_x \cdot \nabla_x \mathbf u - \boldsymbol \eta_v \cdot \nabla_v \mathbf u\,,
\\[2mm]
\delta f &= - \nabla_x \cdot(\boldsymbol \eta_x f) - \nabla_v \cdot (\boldsymbol \eta_v f )\,.
\end{align}
$$

where $(\boldsymbol \eta_x, \boldsymbol \eta_v)$ are arbitrary functions. Note that we so not have to compute $\delta \mathbf a$, since $L$ is independent of $\mathbf a$. Taking theses constrained variations of $I$ with respect to $\mathbf u$ and $f$, and sorting in terms of $\boldsymbol \eta_x$ and $\boldsymbol \eta_v$, respectively, yields

$$
\mathbf u = \mathbf v\,,\qquad \mathbf a = -\frac em \left( -\nabla \phi + \mathbf v \times (\nabla \times \mathbf A_0)\right)\,.
$$


### Noether's theorem

The symmetry of $L$ with respect to time transformations is due to the fact that $L$ has no explicit time dependence, i.e. time only appears through the time-dependence of the unknowns. Hence, the following energy is conserved (see for instance the appendix of [Sugama 2000](https://pubs.aip.org/aip/pop/article-abstract/7/2/466/457925/Gyrokinetic-field-theory)):

$$
W_{\textrm{tot}} = \int \mathbf u \cdot \frac{\partial \mathcal L}{\partial \mathbf u}\, \textrm d \mathbf v\,\textrm d \mathbf x - L\,,
$$

where $\mathcal L$ is the Lagrange density such that

$$
 L = \int \mathcal L\,\textrm d \mathbf v\,\textrm d \mathbf x\,.
$$

For the Lagrangian (5) this leads to

$$
W_{\textrm{tot}} = \int \left(\frac{m|\mathbf v|^2}{2} - e \phi \right) f \,\textrm d \mathbf v\,\textrm d \mathbf x - \frac{\epsilon_0}{2}\int_\Omega |\nabla \phi|^2 \textrm d \mathbf x + \int_\Omega \phi\,\rho_{\textrm i 0}\, \textrm d \mathbf x\,,
$$

We can substitute the Poisson equation to obtain

$$
\begin{equation}
W_{\textrm{tot}} = \int \frac{m|\mathbf v|^2}{2}  f \,\textrm d \mathbf v\,\textrm d \mathbf x + \frac{\epsilon_0}{2}\int_\Omega |\nabla \phi|^2 \textrm d \mathbf x\,.
\end{equation}
$$

Let us verify that this energy is indeed conserved:

$$
\begin{aligned}
\dot W_{\textrm{tot}} &= \int \frac{m|\mathbf v|^2}{2}  \partial_t f \,\textrm d \mathbf v\,\textrm d \mathbf x + \epsilon_0\int_\Omega \nabla \phi \cdot \nabla \partial_t \phi\, \textrm d \mathbf x
\\[2mm]
&=\int \frac{m|\mathbf v|^2}{2}  \partial_t f \,\textrm d \mathbf v\,\textrm d \mathbf x - \epsilon_0 \int_\Omega \phi \partial_t \nabla \cdot \nabla \phi\, \textrm d \mathbf x
\\[2mm]
&=\int \frac{m|\mathbf v|^2}{2}  \partial_t f \,\textrm d \mathbf v\,\textrm d \mathbf x +  \int_\Omega \phi \partial_t \left(\rho_{\textrm i 0} - e\int f \,\mathrm d \mathbf v)\right)\, \textrm d \mathbf x
\\[2mm]
&=\int \left(\frac{m|\mathbf v|^2}{2}  - e \phi \right)\partial_t f \,\textrm d \mathbf v\,\textrm d \mathbf x
\,.
\end{aligned}
$$

Denoting $H:= \frac{|\mathbf v|^2}{2}  - \frac e m \phi$ we realize that the Vlasov equation can be written as

$$
 \partial_t f - [H, f] - \frac e m \,(\mathbf v \times \mathbf B_0)\cdot \nabla_v f = 0\,,
$$

which was written in terms of the canonical Poisson bracket. 

$$
[H, f] := \nabla_x H \cdot \nabla_v f - \nabla_v H \cdot \nabla_x f\,.
$$

Now using the fact that, under the integral,

$$
\int H [H, f]\,\textrm d \mathbf v\,\textrm d \mathbf x = 0\,,
$$ 

which can be easily verified through integration by parts, we find

$$
\begin{aligned}
\dot W_{\textrm{tot}}  &=m \int H[H, f] \,\textrm d \mathbf v\,\textrm d \mathbf x + e\int H \,(\mathbf v \times \mathbf B_0)\cdot \nabla_v f \,\textrm d \mathbf v\,\textrm d \mathbf x
\\[2mm]
&= - e \int \nabla_v H \cdot (\mathbf v \times \mathbf B_0) f \,\textrm d \mathbf v\,\textrm d \mathbf x
\\[2mm]
&= - e\int \mathbf v \cdot (\mathbf v \times \mathbf B_0) f \,\textrm d \mathbf v\,\textrm d \mathbf x 
\\[3mm]
&= 0\,.
\end{aligned}
$$

## Energy-conserving semi-discretization of Vlasov-Poisson with $\phi$

With the [GEMPIC framework of Struphy](https://struphy.pages.mpcdf.de/struphy/sections/discretization.html) it is straigthforward to obtain an energy-conserving semi-discretization in space of (1)-(2). First, let us write the weak form of the Poisson equation ($L^2$-scalar product with test function $\psi \in H^1$ and integration by parts on the left):

$$
 \epsilon_0 \int \nabla \phi \cdot \nabla \psi\,\mathrm d \mathbf x = \int \rho_{\textrm i 0} \psi \,\mathrm d \mathbf x - e \int f \psi \,\textnormal d \mathbf v \,\mathrm d \mathbf x\,,\qquad \forall \ \psi \in H^1\,.
$$

Next, we have to use the mapping $F: \boldsymbol \eta \mapsto \mathbf x$ and the transformation rule of the gradient,

$$
\nabla_x \phi(\mathbf x) = DF^{-\top} \hat\nabla_\eta \hat \phi(\boldsymbol \eta)\,,
$$

where $DF: \boldsymbol \eta \mapsto \mathbb R^{3\times 3}$ denotes the Jacobian matrix of the mapping and $\hat \phi(\boldsymbol \eta) := \phi(F(\boldsymbol \eta))$ is the transformation of 0-forms, to pull back the weak Poisson equation to the logical unit cube:

$$
\begin{equation}
 \epsilon_0 \int \hat\nabla \hat\phi^\top G^{-1}  \hat\nabla \hat\psi\,\sqrt g\,\mathrm d \boldsymbol \eta = \int \hat\rho_{\textrm i 0} \hat\psi \,\sqrt g\,\mathrm d \boldsymbol \eta - e \int \hat f^{\textrm{vol}} \hat\psi \,\textnormal d \mathbf v \,\mathrm d \boldsymbol \eta\,,\qquad \forall \ \hat\psi \in H^1\,.
 \end{equation}
$$

Here, $G = DF^\top DF$ denotes the metric tensor, $\sqrt g = |\textrm{det}\, DF|$ stands for the Jacobian determinant and $\hat f^{\textrm{vol}} = \hat f \sqrt g$ is the volume form corresponding to $f$. The electrostatic potential is discretized as a 0-form:

$$
\hat \phi \approx \hat\phi_h(t, \boldsymbol \eta) = \sum_{i=0}^{N_0 - 1} \phi_i(t)\,\Lambda^0_i(\boldsymbol \eta) = \boldsymbol \phi^\top \boldsymbol \Lambda^0(\boldsymbol \eta) \quad \in \ V^0_h \subset H^1\,.
$$

Its discrete gradient lives in $V^1_h \subset H(\textrm{curl})$ and can be written as

$$
\begin{equation} 
\hat \nabla \hat\phi_h(t, \boldsymbol \eta) = \sum_{i=0}^{N_1-1} (\mathbb G \boldsymbol \phi(t))_i \,\Lambda^1_i(\boldsymbol \eta)\quad \in \ V^1_h \subset H(\textrm{curl})\,,
\end{equation}
$$

where $\mathbb G \in \mathbb R^{N_1 \times N_0}$ is the gradient matrix.
We can thus already write the first two terms in (12) in discrete form:

$$
\begin{equation}
\begin{aligned}
 \epsilon_0 \boldsymbol \psi^\top \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi = \boldsymbol \psi^\top \int \hat\rho_{\textrm i 0} \boldsymbol \Lambda^0 \sqrt g\,\mathrm d \boldsymbol \eta  
 \\[2mm]
 - e \boldsymbol \psi^\top\int \hat f^{\textrm{vol}} \boldsymbol \Lambda^0 \,\textnormal d \mathbf v \,\mathrm d \boldsymbol \eta\,,\qquad \forall \ \boldsymbol\psi \in \mathbb R^{N_0}\,.
 \end{aligned}
 \end{equation}
$$

Here, bold symbols such as $\boldsymbol \phi = (\phi_i)_{i=0}^{N_0 - 1} \in \mathbb R^{N_0}$ indicate vectors, with the Euclidean scalar product denoted by $(\boldsymbol \psi,  \boldsymbol \phi) = \boldsymbol \psi^\top \boldsymbol \phi$ and we introduced the mass matrix $\mathbb M^1 \in \mathbb R^{N_1 \times N_1}$,

$$
\begin{align}
 \mathbb M^1_{i,j} = \int \Lambda^1_i G^{-1}  \Lambda^1_j\,\sqrt g\,\mathrm d \boldsymbol \eta\,.
\end{align}
$$


The electrons are discretized with $N$ particles:

$$
\hat f^{\textrm{vol}} \approx \hat f^{\textrm{vol}}_h(t, \boldsymbol \eta, \mathbf v) = \frac 1 N\sum_{k=0}^{N-1} w_k \,\delta(\boldsymbol \eta - \boldsymbol \eta_k(t)) \,\delta(\mathbf v - \mathbf v_k(t))\,,
$$

where $(\boldsymbol \eta_k(t), \mathbf v_k(t))$ satisfy the ODEs (6)-(7) on the logical domain:

$$
\begin{align}
 \dot{\boldsymbol \eta}_k &= DF^{-1}(\boldsymbol \eta_k) \mathbf v_k\,,
 \\[2mm]
 \dot{\mathbf v}_k &= \frac e m \left( DF^{-\top}(\boldsymbol \eta_k)\hat\nabla \hat\phi_h(t, \boldsymbol \eta_k) - \mathbf v_k \times \frac{DF}{\sqrt g}(\boldsymbol \eta_k)\hat{\mathbf B}^2_0(\boldsymbol \eta_k)\right)\,.
\end{align}
$$

The third term in the discrete Poisson equation (12) becomes

$$
 - e \int \hat f^{\textrm{vol}} \hat\psi \,\textnormal d \mathbf v \,\mathrm d \boldsymbol \eta \approx - \boldsymbol \psi^\top e \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t))\,.
$$

Hence, the discrete Poisson equation reads

$$
\begin{equation}
 \epsilon_0 \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi = \int \hat\rho_{\textrm i 0} \boldsymbol \Lambda^0 \sqrt g\,\mathrm d \boldsymbol \eta  - e \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t))\,.
\end{equation}
$$

The discrete version of the energy (11) reads

$$
\begin{equation}
W_{\textrm{tot},h}(t) = \frac 1 N \sum_{k=1}^{N-1} w_k \frac{m|\mathbf v_k(t)|^2}{2} + \frac{\epsilon_0}{2} \boldsymbol \phi^\top(t) \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi(t)\,.
\end{equation}
$$

Let us verify that this semi-discrete energy is indeed conserved:

$$
\begin{aligned}
\dot W_{\textrm{tot},h} &= \frac 1 N \sum_{k=1}^{N-1} w_k m\,\mathbf v_k^\top  \dot{\mathbf v}_k + \epsilon_0 \boldsymbol \phi^\top \mathbb G^\top\mathbb M^1\mathbb G\, \dot{\boldsymbol \phi}
\\[2mm]
&=e\frac 1 N \sum_{k=1}^{N-1} w_k \,\mathbf v_k^\top  \left[ DF^{-\top}\left( \hat\nabla \hat\phi_h(t, \boldsymbol \eta_k) - DF^{-1}\mathbf v_k \times \hat{\mathbf B}^2_0(\boldsymbol \eta_k)\right) \right]
\\[2mm]
&\qquad\qquad  + \boldsymbol \phi^\top \frac{\mathrm d}{\mathrm d t} \left( \int \hat\rho_{\textrm i 0} \boldsymbol \Lambda^1 \sqrt g\,\mathrm d \boldsymbol \eta - e\frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k(t)) \right)
\\[2mm]
&= e\frac 1 N \sum_{k=1}^{N-1} w_k \,\mathbf v_k^\top   DF^{-\top} \hat\nabla \hat\phi_h(t, \boldsymbol \eta_k) - \boldsymbol \phi^\top e \frac 1 N \sum_{k=0}^{N-1} w_k \dot{ \boldsymbol \eta}_k(t)^\top \nabla \boldsymbol \Lambda^0(\boldsymbol \eta_k(t))
\\[2mm]
&= e\frac 1 N \sum_{k=1}^{N-1} w_k \,\mathbf v_k^\top   DF^{-\top} \hat\nabla \hat\phi_h(t, \boldsymbol \eta_k) - e\frac 1 N \sum_{k=1}^{N-1} w_k \,\mathbf v_k^\top   DF^{-\top} \hat\nabla \hat\phi_h(t, \boldsymbol \eta_k) 
\\[4mm]
&= 0\,.
\end{aligned}
$$

## Hamiltonian structure

It is straightforward to write the Vlasov-Poisson system as a non-canonical Hamiltonian system in the variables $\mathbf X = (\boldsymbol \eta_k)_{k=0}^{N-1} \in \mathbb R^{3N}$ and $\mathbf V = (\mathbf v_k)_{k=0}^{N-1} \in \mathbb R^{3N}$, where the electric potantial $\phi(\mathbf X)$ is viewed as a complicated function of $\mathbf X$ related to the inverse Laplacian. The discrete energy (19) is the Hamiltonian,  written as

$$
\begin{equation}
H(\mathbf X, \mathbf V) = \frac m2 \mathbf V^\top \mathbb N \mathbf V + \frac{\epsilon_0}{2} \boldsymbol \phi^\top(\mathbf X) \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi(\mathbf X)\,,
\end{equation}
$$

where 

$$
\mathbb N = \textrm{diag}(\mathbf w) \otimes \mathbb I_{3\times 3} \in \mathbb R^{3N \times 3N}\,,\qquad \mathbf w = \left( \frac{w_k}{N} \right)_{k=0}^{N-1} \in \mathbb R^N\,. 
$$

Moreover, the discrete Poisson equation (18) is written as

$$
\begin{equation}
 \epsilon_0 \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi(\mathbf X) = \boldsymbol \rho_{\textrm i 0}  - e\, \mathbb L^0(\mathbf X) \mathbf w\,,
\end{equation}
$$

with 

$$
 \boldsymbol \rho_{\textrm i 0} = \int \hat\rho_{\textrm i 0} \boldsymbol \Lambda^0 \sqrt g\,\mathrm d \boldsymbol \eta \in \mathbb R^{N_0}\,,\qquad \mathbb L^0(\mathbf X) \in \mathbb R^{N_0 \times N}\,,\qquad \mathbb L^0_{ik}(\mathbf X) = \Lambda^0_i(\boldsymbol \eta_k) \in \mathbb R\,.
$$

The gradient of the Hamiltonian (20) with respect to $\mathbf X$ can be computed with the help of the discrete Poisson equation (21),

$$
\begin{equation}
 \frac{\partial}{\partial \mathbf X} H(\mathbf X, \mathbf V) = \boldsymbol \phi^\top(\mathbf X)\frac{\partial}{\partial \mathbf X}\epsilon_0 \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi(\mathbf X) =  - e \boldsymbol \phi^\top(\mathbf X) \frac{\partial}{\partial \mathbf X} \mathbb L^0(\mathbf X) \mathbf w = -e (\mathbb G \boldsymbol \phi(\mathbf X))^\top\mathbb L^1(\mathbf X)\, \mathbb N\,,
\end{equation}
$$

where we used

$$
\frac{\partial}{\partial \boldsymbol \eta_l} \mathbb L^0_i(\mathbf X) \mathbf w = \frac{\partial}{\partial \boldsymbol \eta_l} \frac 1 N \sum_{k=0}^{N-1} w_k \Lambda_i^0(\boldsymbol \eta_k) = \frac 1 N \sum_{k=0}^{N-1} w_k \frac{\partial \boldsymbol \eta_k}{\partial \boldsymbol \eta_l} \cdot \hat\nabla\Lambda_i^0(\boldsymbol \eta_k) = \frac 1 N w_l\,\mathbb I_{3\times 3} \hat\nabla\Lambda_i^0(\boldsymbol \eta_l)
$$

as well as the relation (13) for gradients, along with the definition

$$
\mathbb L^1(\mathbf X) \in \mathbb R^{N_1 \times N \times 3}\,,\qquad \mathbb L^1_{ik} = \Lambda^1_i(\boldsymbol \eta_k) \in \mathbb R^3 \ (\textnormal{vector-valued basis functions})\,.
$$

With this we can compute the gradient of the Hamiltonian w.r.t $(\mathbf X, \mathbf V)$,

$$
\nabla H(\mathbf X, \mathbf V) = 
\begin{pmatrix}
 -e (\mathbb G \boldsymbol \phi(\mathbf X))^\top\mathbb L^1(\mathbf X)\, \mathbb N 
\\[1mm]
m \mathbb N \mathbf V
\end{pmatrix}\,.
$$

Hence, we are able to write the equations of motion (16)-(17) as a non-canonical Hamiltonian system:

$$
\begin{equation}
 \begin{pmatrix}
 \dot{\mathbf X} \\
 \dot{\mathbf V}
  \end{pmatrix} = 
  \begin{pmatrix}
   0 & \frac 1 m (\mathbb I_{N\times N}\otimes DF^{-1}(\mathbf X)) \,\mathbb N^{-1}
   \\[1mm]
   - \frac 1 m \mathbb N^{-1}(\mathbb I_{N \times N} \otimes DF^{-\top}(\mathbf X))  & \mathbb B_{0\times}(\mathbf X)
  \end{pmatrix}
  \nabla H(\mathbf X, \mathbf V)\,,
\end{equation}
$$
where the diagonal block is the skew-symmetric rotation matrix

$$
\mathbb B_{0\times}(\mathbf X) = \frac{e}{m^2} \mathbb N^{-1} \textrm{diag}\left(  
\frac{DF(\boldsymbol \eta_k)}{\sqrt g(\boldsymbol \eta_k)}\begin{pmatrix}
0 & -B^2_{0,3}(\boldsymbol \eta_k) & B^2_{0,2}(\boldsymbol \eta_k)
\\[1mm]
B^2_{0,3}(\boldsymbol \eta_k) & 0 & -B^2_{0,1}(\boldsymbol \eta_k)
\\[1mm]
-B^2_{0,2}(\boldsymbol \eta_k) & B^2_{0,1}(\boldsymbol \eta_k) & 0 
\end{pmatrix}
\right) \in \mathbb R^{3N \times 3N}\,. 
$$

Note that $\mathbb N^{-1}$ is diagonal and commutes with all matrices.

Reference: [Gu et al. 2022](https://www.sciencedirect.com/science/article/pii/S0021999122005344).

## Time discretization 

The insight that the Vlasov-Poisson system has the Hamiltonian structure (23) can help with the time discretization. We could leverage many techniques for discretizing non-canonical Hamiltonian ODEs (also called "Poisson systems") that lead to structure-preserving algorithms. Unfortunately, our system (23) has the most general form of a Poisson system; denoting $\mathbf Z = (\mathbf X, \mathbf V)$ we have

$$
\dot{\mathbf Z} = \mathbb J(\mathbf Z) \nabla H(\mathbf Z)\,,
$$

where the Poisson matrix $\mathbb J(\mathbf Z)$ depends non-linearly on $\mathbf Z$ through $DF^{-1}(\mathbf X)$ and $\mathbb B_{0\times}(\mathbf X)$. A Poisson integrator (i.e. a Poisson map that respects all Casimirs of $\mathbb J(\mathbf Z)$, see for instance [Hairer, Lubich, Wanner](https://link.springer.com/book/10.1007/3-540-30666-8)) will thus be very hard to obtain, and we don't know how exactly to do that (it is not the purpose of this project). However, we can still go for exact energy conservation with the **discrete gradient method** (see [chapter 8.2 of Michael_2019 lecture notes](https://datashare.mpcdf.mpg.de/apps/files/?dir=/struphy/reference_books&fileid=231001917#pdfviewer)). For this, our first strategy will be to split the Poisson matrix as

$$
 \mathbb J(\mathbf Z) = \mathbb J_1(\mathbf Z) + \mathbb J_2(\mathbf Z)
$$

such that

$$
\begin{aligned}
 \mathbb J_1(\mathbf Z) & = \begin{pmatrix}
   0 & 0
   \\[1mm]
   0  & \mathbb B_{0\times}(\mathbf X)
  \end{pmatrix}\,,
  \\[4mm]
  \mathbb J_2(\mathbf Z) &= \begin{pmatrix}
   0 & \frac 1 m (\mathbb I_{N\times N}\otimes DF^{-1}(\mathbf X)) \,\mathbb N^{-1}
   \\[1mm]
   - \frac 1 m \mathbb N^{-1}(\mathbb I_{N \times N} \otimes DF^{-\top}(\mathbf X))  & 0
  \end{pmatrix}\,.
\end{aligned}
$$

The first splitting step of our scheme is thus a discrete version of

$$
\begin{equation}
 \begin{pmatrix}
 \dot{\mathbf X} \\
 \dot{\mathbf V}
  \end{pmatrix} = 
  \begin{pmatrix}
   0 & 0
   \\[1mm]
   0  & \mathbb B_{0\times}(\mathbf X)
  \end{pmatrix}
  \nabla H(\mathbf X, \mathbf V)\,.
\end{equation}
$$

We note that $\mathbf X$ and thus $\mathbb J_1(\mathbf X)$ remain constant in this step ($\dot{\mathbf X} = 0$), and that $H$ is quadratic in $\mathbf V$; this enables a simple energy-conserving scheme based on the mid-point rule:

$$
 \frac{\mathbf V^{n+1} - \mathbf V^n}{\Delta t} = \mathbb B_{0\times}(\mathbf X^n) \nabla_{\mathbf V}H\left( \mathbf X^n, \frac{\mathbf V^{n+1} + \mathbf V^n}{2}\right)\,.
$$

For a single particle, this amounts to

$$
 \frac{\mathbf v_k^{n+1} - \mathbf v_k^n}{\Delta t} = -\frac e m \left(\frac{\mathbf v_k^{n+1} + \mathbf v_k^n}{2} \right) \times \frac{DF}{\sqrt g}(\boldsymbol \eta_k^n) \,\hat{\mathbf B}_0^2(\boldsymbol \eta_k^n)\,,
$$

which can be solved for each $\mathbf v_k^{n+1}$ in is implemented in the Struphy propagator [PushVxB](https://struphy.pages.mpcdf.de/struphy/sections/inventory.html#struphy.propagators.propagators_markers.PushVxB), where also an analytic solution of (24) is available. 

For the second splitting step we shall try three different schemes, in ascending order of complexity:

### 1. Simple extension of the toy model [Vlasov](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.Vlasov)

The toy model [Vlasov](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.Vlasov) consists of the following two propagators:

a. [PushVxB](https://struphy.pages.mpcdf.de/struphy/sections/inventory.html#struphy.propagators.propagators_markers.PushVxB) for the split step 1.

b. [PushEta](https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_markers.PushEta) for the position update.

We shall create a model `VlasovPoissonSimple` by adding two propagators to this toy model:

c. [StepVinEfield](https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_markers.StepVinEfield) for updating the velocities in a given electric field, with fixed positions. This propagator has to be cleaned up and renamed to `PushVinEfield`. The E-field has to be computed from the current FE coefficients of the potantial by applying the gradient matrix.

d. A new propagator added to `propagators_coupling.py`, maybe called `Poisson`, that basically calls [ImplicitDiffusion](https://struphy.pages.mpcdf.de/struphy/sections/propagators.html#struphy.propagators.propagators_fields.ImplicitDiffusion), with the appropriate right hand side obtained from an [Accumulator](https://struphy.pages.mpcdf.de/struphy/sections/accumulators.html#struphy.pic.accumulation.particles_to_grid.Accumulator) for the particle deposition to the "grid" (FE coefficients).

### 2. An energy-conserving scheme for linear splines

A "simple" but not exact approach could be through the time discretization of the energy conservation proof above. For the energy (19) at consecutive time steps we have

$$
\begin{equation}
\begin{aligned}
\frac{W_{\textrm{tot},h}^{n+1} - W_{\textrm{tot},h}^{n}}{\Delta t} &= \frac 1 N \sum_{k=1}^{N-1} w_k \frac{m \left( |\mathbf v_k^{n+1}|^2 - |\mathbf v_k^{n}|^2 \right)}{2 \Delta t} + \frac{\epsilon_0}{2 \Delta t} \boldsymbol \phi^{n+1,\top} \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi^{n+1}
\\[2mm]
&\qquad\qquad\qquad\qquad\qquad\qquad\qquad - \frac{\epsilon_0}{2 \Delta t} \boldsymbol \phi^{n,\top} \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi^{n}
\\[2mm]
&=\frac 1 N \sum_{k=1}^{N-1} w_k m\frac{ \left( \mathbf v_k^{n+1} + \mathbf v_k^{n} \right)^\top}{2} \frac{ \left( \mathbf v_k^{n+1} - \mathbf v_k^{n} \right)}{ \Delta t} + \epsilon_0 \frac{\boldsymbol \phi^{n+1,\top} + \boldsymbol\phi^{n,\top}}{2} \mathbb G^\top\mathbb M^1\mathbb G\, \frac{\boldsymbol \phi^{n+1} - \boldsymbol \phi^{n}}{\Delta t}\,.
\end{aligned}
\end{equation}
$$

Moreover, from the discrete Poisson equation (18),

$$
\begin{aligned}
 \epsilon_0G^\top\mathbb M^1\mathbb G\, (\boldsymbol \phi^{n+1} - \boldsymbol \phi^{n}) &= - e \frac 1 N \sum_{k=0}^{N-1} w_k \left(\boldsymbol \Lambda^0(\boldsymbol \eta_k^{n+1}) - \boldsymbol \Lambda^0(\boldsymbol \eta_k^{n}) \right)\,.
\end{aligned}
$$

This gives us a first hint towards a nice discretization, namely we could try:

$$
\begin{align}
\frac{ \boldsymbol \eta_k^{n+1} - \boldsymbol \eta_k^{n} }{ \Delta t} &= DF^{-1} \frac{\mathbf v_k^{n+1} + \mathbf v_k^{n}}{2} \,.
\\[2mm]
\frac{ \mathbf v_k^{n+1} - \mathbf v_k^{n} }{ \Delta t} &= \frac e m DF^{-\top} \frac{(\mathbb G\boldsymbol \phi^{n+1} + \mathbb G \boldsymbol\phi^{n})^\top} {2} \boldsymbol \Lambda^1(\boldsymbol \eta_k^n) \,.
\end{align}
$$

Taylor expansion yields

$$
 \boldsymbol \Lambda^0(\boldsymbol \eta_k^{n+1}) - \boldsymbol \Lambda^0(\boldsymbol \eta_k^{n}) = \Delta t \left(DF^{-1}(\boldsymbol \eta_k) \frac{\mathbf v_k^{n+1} + \mathbf v_k^{n}}{2} \right)^\top\nabla \boldsymbol \Lambda^0(\boldsymbol \eta_k^n) + O(\Delta t^2)
$$

Substituting the above in (25) shows that the ernergy is conserved up to $O(\Delta t^2)$; for linear splines energy is conserved because the Taylor expansion is exact. The drawback is of course the increased cost of the scheme, because it is fully implicit. This is a general feature of energy-conserving schemes. A simple Picard iteration scheme would look like this:

$$
\begin{align}
\frac{ \boldsymbol \eta_k^{n+1, l+1} - \boldsymbol \eta_k^{n} }{ \Delta t} &= DF^{-1} \frac{\mathbf v_k^{n+1, l} + \mathbf v_k^{n}}{2} \,.
\\[2mm]
\frac{ \mathbf v_k^{n+1, l+1} - \mathbf v_k^{n} }{ \Delta t} &= \frac e m DF^{-\top} \frac{(\mathbb G\boldsymbol \phi^{n+1, l} + \mathbb G \boldsymbol\phi^{n})^\top} {2} \boldsymbol \Lambda^1(\boldsymbol \eta_k^n) 
\\[2mm]
\epsilon_0 \mathbb G^\top\mathbb M^1\mathbb G\, \boldsymbol \phi^{n+1, l+1} &= \int \hat\rho_{\textrm i 0} \boldsymbol \Lambda^0 \sqrt g\,\mathrm d \boldsymbol \eta  - e \frac 1 N \sum_{k=0}^{N-1} w_k \boldsymbol \Lambda^0(\boldsymbol \eta_k^{n+1, l})\,,
\end{align}
$$

where $l\geq 0$ denotes the iteration index and $(\mathbf X^{n+1, 0}, \mathbb V^{n+1, 0}, \boldsymbol \phi^{n+1, 0}) = (\mathbf X^{n}, \mathbb V^{n}, \boldsymbol \phi^{n})$. for this, we need to implement a new model `VlasovPoissonImplicit` with a new propagator `VlasovPoissonImplicit`.



### 3. Discrete gradient method

This will be even more expensive than scheme 2 but will feature exact energy conservation. We shall implement the Gonzalez discrete gradient method for substep 2:

$$
\begin{equation}
 \frac{\mathbf Z^{n+1} - \mathbf Z^n}{\Delta t} = \mathbb J_2(\mathbf Z^n) \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^n)\,,
\end{equation}
$$

where

$$
\begin{equation}
 \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^n) = \nabla H(\mathbf Z^{n+1/2}) + (\mathbf Z^{n+1} - \mathbf Z^n) \frac{H(\mathbf Z^{n+1}) - H(\mathbf Z^n) - (\mathbf Z^{n+1} - \mathbf Z^{n+1})^\top \nabla H(\mathbf Z^{n+1/2}) }{||\mathbf Z^{n+1} - \mathbf Z^n||}\,,
\end{equation}
$$

and $\mathbf Z^{n+1/2} = (\mathbf Z^{n+1}  +\mathbf Z^{n})/2$.
Like all discrete gradients, but here particularly easy to see, the Gonzalez discrete gradient satisfies

$$
 (\mathbf Z^{n+1} - \mathbf Z^{n+1}) \overline \nabla H(\mathbf Z^{n+1}, \mathbf Z^{n}) = H(\mathbf Z^{n+1}) - H(\mathbf Z^n)\,,
$$

which immediatly implies exact conservation of $H$ through the skew symmetry of $\mathbb J_2$ in (31). 

Steps to take:

a. Write down the ensuing discrete equations of motion 

b. create a new model `VlasovPoissonDiscreteGradient` with the new propagator `VlasovPoissonDiscreteGradient`.



## Lagrangian with $A$

Will be added later.


## Info

* Project executive(s): Richard Vanderburgh
* Duration: 6 months
* Context: community  
* Funding: none
* Start date: 01/2024
* End date: open
* Supervisor: [Stefan Possanner](https://www.linkedin.com/in/stefan-possanner-52a1a250/)

## Links

* [Struphy doc](https://struphy.pages.mpcdf.de/struphy/index.html)

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)

 
